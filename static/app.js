// Build Sudoku Grid
for (var i = 0; i < 9; i++) {
  var div = '<div class="row' + i + '"></div>'
  $('.sudoku-grid').append(div)
  for (var j = 0; j < 9; j++) {
    var text_field = '<input type="text" id="space' + i + j + '" name="" value="" class="sudoku-space" oninput="this.value = this.value.replace(/[^0-9.]/g, ``).replace(/(\..*)\./g, `$1`);">'
    $('.row' + i).append(text_field)
  }
  $('.row' + i).append('<br>')
}

$('.sudoku-space').each(function(i, el) {
  var classList = $(this).attr('id').split('space')
  var spaceId = classList[classList.length - 1]
  var firstDigit = spaceId[0]
  var secondDigit = spaceId[1]
  var isGray = firstDigit < 3 && secondDigit < 3
  isGray += firstDigit < 3 && secondDigit > 5
  isGray += (firstDigit >= 3 && firstDigit <= 5) && (secondDigit >= 3 && secondDigit <= 5)
  isGray += firstDigit > 5 && secondDigit < 3
  isGray += firstDigit > 5 && secondDigit > 5
  if (isGray) {
    $(this).addClass('gray')
  }
})

function clearBoard() {
  $('.sudoku-space').val('')
}

function getBoardFromFields() {
  $(".sudoku-space").prop('disabled', false);
  var spaces = [];
  var row = [];
  $('.sudoku-space').each(function(id, space) {
    if (id % 9 == 0 && id > 0) {
      spaces.push(row)
      row = []
    }
    if (space.value == '') {
      row.push(null)
    } else {
      row.push(parseInt(space.value))
    }

  })
  spaces.push(row)
  return spaces;
}

function solve() {
  var spaces = getBoardFromFields()
  $.ajax({
      url: "/solve",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({"board": spaces})
    }).done(function(data) {
      showSolvedBoard(data)
  });
}

function showSolvedBoard(data) {
  if (data.invalid) {
    displayError(data.invalid)
  } else {
    clearChanges()
    board = data.board
    board.forEach(function(row, i) {
      row.forEach(function(space, j) {
        $('#space' + i.toString() + j.toString()).val(board[i][j])
      })
    })
    if (data.solved) {
      displaySolved()
    } else {
      $('#space' + moveString).addClass('font-blue')
    }
  }
}

function getMove() {
  var spaces = getBoardFromFields()

  $.ajax({
      url: "/one_move",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({"board": spaces})
    }).done(function(data) {
      placeMove(data)
  });
}

function clearChanges() {
  $('.sudoku-space').removeClass('font-red font-blue font-green')
  $('.error-message').addClass('invisible')
}

function placeMove(data) {
  if (data.invalid) {
    displayError(data.invalid)
  } else {
    move = data.move.space
    board = data.board
    newValue = board[move[0]][move[1]]
    moveString = move.join('')
    clearChanges()
    $('#space' + moveString).val(newValue)
    if (data.solved) {
      displaySolved()
    } else {
      $('#space' + moveString).addClass('font-blue')
    }

  }
}

function displaySolved() {
  $('.sudoku-space').addClass('font-green')
}

function displayError(invalidCoords) {
  clearChanges()
  $('.error-message').removeClass('invisible')
  invalidCoords.forEach(function(invalidCoord) {
    invalidCoordString = invalidCoord.join('')
    $('#space' + invalidCoordString).addClass('font-red')
  })
}

$.getScript('static/boardSamples.js')
