function fillWithSample() {
  clearBoard()
  clearChanges()
  $('#space01').val(9)
  $('#space02').val(5)
  $('#space03').val(1)
  $('#space07').val(8)

  $('#space10').val(1)
  $('#space12').val(7)
  $('#space13').val(9)
  $('#space14').val(8)
  $('#space16').val(4)
  $('#space17').val(5)
  $('#space18').val(2)

  $('#space21').val(8)
  $('#space26').val(9)
  $('#space27').val(6)

  $('#space33').val(8)
  $('#space34').val(4)
  $('#space35').val(2)
  $('#space37').val(9)

  $('#space44').val(9)
  $('#space45').val(5)
  $('#space46').val(6)
  $('#space48').val(4)

  $('#space50').val(5)
  $('#space55').val(7)

  $('#space61').val(6)
  $('#space66').val(5)

  $('#space72').val(1)
  $('#space76').val(2)
  $('#space78').val(9)

  $('#space80').val(7)
}

function fillWithUnsolvable() {
  clearBoard()
  clearChanges()
  $('#space01').val(1)
  $('#space02').val(3)
  $('#space04').val(6)
  $('#space05').val(2)
  $('#space06').val(5)

  $('#space11').val(9)
  $('#space13').val(8)
  $('#space17').val(3)

  $('#space20').val(4)
  $('#space22').val(6)
  $('#space24').val(5)
  $('#space28').val(2)

  $('#space32').val(5)
  $('#space35').val(7)
  $('#space36').val(9)
  $('#space38').val(8)

  $('#space43').val(4)
  $('#space45').val(8)
  $('#space46').val(7)

  $('#space57').val(1)

  $('#space64').val(4)
  $('#space65').val(9)

  $('#space70').val(6)
  $('#space71').val(7)
  $('#space73').val(2)

  $('#space88').val(9)
}
